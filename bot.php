<?php
/**
 * Created by PhpStorm.
 * User: hacker
 * Date: 7/2/18
 * Time: 7:38 AM
 */

require_once 'engine/Config.php';
require_once 'engine/Response.php';

setBotConfiguration();

$access_token = getAccessToken();

$input = json_decode(file_get_contents('php://input'), true);
$sender_id = $input['entry'][0]['messaging'][0]['sender']['id'];
$query = strtolower($input['entry'][0]['messaging'][0]['message']['text']);

$url = "https://graph.facebook.com/v3.0/me/messages?access_token=$access_token";

if(!empty($query))
{
    $mark_seen = '{
          "recipient":{
            "id":"'.$sender_id.'"
          },
          "sender_action":"mark_seen"
        }';
    
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $mark_seen);
    curl_setopt($curl, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
    curl_exec($curl);
    curl_close($curl);
    
    $typing_on = '{
          "recipient":{
            "id":"'.$sender_id.'"
          },
          "sender_action":"typing_on"
        }';
    
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $typing_on);
    curl_setopt($curl, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
    curl_exec($curl);
    curl_close($curl);
    
    
    $response = getResponse($sender_id, $query);
    
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $response);
    curl_setopt($curl, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
    curl_exec($curl);
    curl_close($curl);
    
}
