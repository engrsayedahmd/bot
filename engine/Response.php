<?php
/**
 * Created by PhpStorm.
 * User: hacker
 * Date: 7/2/18
 * Time: 7:56 AM
 */

require_once 'tmp/Templates.php';
require_once 'db/db.php';
/*
function getResponse($senderID, $query)
{
    $response = '';
    
    switch($message)
    {
        case 'hi':
            $text = 'Hello';
            $response = SendText($senderID, $text);
            break;
        case 'hello':
            $text = 'Hi';
            $response = SendText($senderID, $text);
            break;
        case 'file':
            $type = 'file';
            $url = 'https://sayed.xyz/messenger/bot.php.zip';
            $response = AttachingFromURL($senderID, $type, $url);
            break;
        case 'image':
            $type = 'image';
            $url = 'https://yt3.ggpht.com/a-/ACSszfF93MaowmyxkddvAdE1qe6MY5WMh4WxWgVDWw=s900-mo-c-c0xffffffff-rj-k-no';
            $response = AttachingFromURL($sender_id, $type, $url);
            break;
        case 'quick reply':
                $text = 'Greetings!';
                $quick_replies = array(
                    array(
                        'content_type' => 'text',
                        'title' => 'HI',
                        'payload' => 'hi'
                        ),
                    array(
                        'content_type' => 'text',
                        'title' => 'HELLO',
                        'payload' => 'hello'
                        )
                    );
            $response = SendingQuickReplies($senderID, $text, $quick_replies);
            break;
        default:
            $text = 'Can not understand your query.';
            $response = SendText($senderID, $text);
    }
        
    return $response;
}*/

function getQuickReplies($query)
{
    global $db;
    $quick_replies = array();
    $text = "";
    
    $sql = "SELECT text, content_type, title, payload FROM quick_reply WHERE query=?";
    $stmt = $db->prepare($sql);
    $stmt->execute(array($query));
    $res = $stmt->fetchAll(2);
    
    foreach($res as $quick_reply)
    {
        $text = $quick_reply['text'];
        
        $tmp = array(
            'content_type' => $quick_reply['content_type'],
            'title' => $quick_reply['title'],
            'payload' => $quick_reply['payload'],
            );
            
        array_push($quick_replies, $tmp);
        
    }
    
    return array('text'=> $text, 'quick_replies' => $quick_replies);
    
}

function getMessage($query)
{
    global $db;

    $sql = "SELECT response FROM message WHERE query=?";
    $stmt = $db->prepare($sql);
    $stmt->execute(array($query));
    $res = $stmt->fetch(2);
    return $res['response']; 
}

function getTaggingResponse($sender_id, $query, $tag)
{
   global $db;
    
    switch($tag)
    {
        case 'message':
            $text = getMessage($query);
            return sendText($sender_id, $text);
            break;
        case 'quick_reply':
            $quick_replies = getQuickReplies($query);
            $text = $quick_replies['text'];
            return sendingQuickReplies($sender_id, $text, $quick_replies['quick_replies']);
        default:
            $text = 'Can not understand your query';
            return sendText($sender_id, $text);
    }
}

function getResponse($sender_id, $query)
{
    global $db;
    
    $sql = "SELECT tag FROM `tagging` WHERE query=?";
    $stmt = $db->prepare($sql);
    $stmt->execute(array($query));
    $res = $stmt->fetch(2);
    $tag = $res['tag'];
    
    return getTaggingResponse($sender_id, $query, $tag);
}