<?php
/**
 * Created by PhpStorm.
 * User: hacker
 * Date: 7/2/18
 * Time: 7:56 AM
 */


function sendText($sender_id, $text)
{
    $response = "{
        'recipient' : {
            'id' : '$sender_id'
        },
        'message' : {
            'text' : '$text'
        }
    }";

    return $response;

}

function attachingFromURL($sender_id, $type, $file_url)
{
    $response = '{
        "recipient":{
                    "id":"'.$sender_id.'"
                  },
                  "message":{
                    "attachment":{
                      "type":"'.$type.'", 
                      "payload":{
                        "url":"'.$file_url.'", 
                        "is_reusable":false
                      }
                    }
                }
    
    }';
    
    return $response;
                
}