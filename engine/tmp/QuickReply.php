<?php

function sendingQuickReplies($sender_id, $text, $quick_replies)
{
       
    
        $encoded = array();
        
        foreach($quick_replies as $quick_reply)
        {
            array_push($encoded,json_encode($quick_reply));
        }
        
        
        $encoded_quick_replies = implode(',',$encoded);
        
        
        $response = '{
            "recipient":{
                "id":"'.$sender_id.'"
              },
              "message":{
                "text": "'.$text.'",
                "quick_replies":[
                  '.$encoded_quick_replies.'
                ]
              }
        }';
    
    
    
    return $response;
    
}