<?php

require_once 'db/db.php';

# Date Functions
date_default_timezone_set("Asia/Dhaka");


# Get Access Token
function getAccessToken()
{
    global $db;
    
    $sql = "SELECT access_token FROM `config`";
    $stmt = $db->prepare($sql);
    $stmt->execute();
    $res = $stmt->fetch(2);
    $stmt = null;
    
    return $res['access_token'];
}

# Get Hub Token
function getHubToken()
{
    global $db;
    
    $sql = "SELECT hub_verify_token FROM `config`";
    $stmt = $db->prepare($sql);
    $stmt->execute();
    $res = $stmt->fetch(2);
    $stmt = null;
    
    return $res['hub_verify_token'];
}

# Set Bot Configuration
function setBotConfiguration()
{
    if(isset($_REQUEST['hub_challenge'])){
    $challenge = $_REQUEST['hub_challenge'];
    $hub_verify_token = $_REQUEST['hub_verify_token'];
    }
    
    if($hub_verify_token == getHubToken()){
        echo $challenge;
        die();
    }
}
