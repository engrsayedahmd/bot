-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 27, 2018 at 06:55 PM
-- Server version: 5.7.18-1
-- PHP Version: 7.1.6-2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `messenger`
--

-- --------------------------------------------------------

--
-- Table structure for table `config`
--

CREATE TABLE `config` (
  `id` int(10) NOT NULL,
  `access_token` longtext NOT NULL,
  `hub_verify_token` longtext NOT NULL,
  `greeting_text` text NOT NULL,
  `greeting_text_status` int(10) NOT NULL,
  `get_start_status` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `config`
--

INSERT INTO `config` (`id`, `access_token`, `hub_verify_token`, `greeting_text`, `greeting_text_status`, `get_start_status`) VALUES
(1, 'EAAfgTfv7qfgBAMNHazz5tGEz1se58YatoR7QXiRZCm7DWalGwlFAkFH24E8bgPrX5JsqZBm30pbbZAAFZAGcUQwwUKhFrG6YFYYDcXbR9bIq2bEMcU6ZAl2TZCIbgdJoFwkiZCX1CnswbIovEg5LdhEKKE1IrZCX26itMikEtVP2wZCn2LoXnsdni', 'HGfyV450mUGHb', 'Hi, {{user_first_name}} Welcome to Adda Bot!', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE `message` (
  `id` int(11) NOT NULL,
  `query` varchar(255) NOT NULL,
  `response` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `message`
--

INSERT INTO `message` (`id`, `query`, `response`) VALUES
(1, 'hi', 'Hello'),
(2, 'hello', 'Hi, There!'),
(3, 'Good Morning', 'Good Morning Too'),
(8, '', ''),
(7, 'how are you', 'I am fine. And you ?'),
(9, '', ''),
(10, 'hlw', 'Hi\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `quick_reply`
--

CREATE TABLE `quick_reply` (
  `id` int(11) NOT NULL,
  `query` varchar(255) NOT NULL,
  `text` text NOT NULL,
  `content_type` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `payload` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `quick_reply`
--

INSERT INTO `quick_reply` (`id`, `query`, `text`, `content_type`, `title`, `payload`) VALUES
(1, 'greetings', 'Here are greetings!', 'text', 'Hi', 'hi'),
(3, 'greetings', 'Here are greetings!', 'text', 'Hello', 'hello');

-- --------------------------------------------------------

--
-- Table structure for table `tagging`
--

CREATE TABLE `tagging` (
  `id` int(11) NOT NULL,
  `query` varchar(255) NOT NULL,
  `tag` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tagging`
--

INSERT INTO `tagging` (`id`, `query`, `tag`) VALUES
(1, 'hi', 'message'),
(2, 'greetings', 'quick_reply'),
(3, 'hello', 'message'),
(5, 'how are you', 'message'),
(6, '', 'message'),
(7, '', 'message'),
(8, 'hlw', 'message');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `config`
--
ALTER TABLE `config`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quick_reply`
--
ALTER TABLE `quick_reply`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tagging`
--
ALTER TABLE `tagging`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `config`
--
ALTER TABLE `config`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `message`
--
ALTER TABLE `message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `quick_reply`
--
ALTER TABLE `quick_reply`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tagging`
--
ALTER TABLE `tagging`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
