<?php
require_once 'app/fun/functions.php';
require_once 'app/engine-controller.php';

$config = config();

if(isset($_POST['enable_welcome_text']))
{
    enableGreetingText();
    header('Location: engine.php?status=enabled');
}

if(isset($_POST['disable_welcome_text']))
{
    disableGreetingText();
    header('Location: engine.php?status=disabled');
}

if(isset($_POST['update_greeting_text']))
{
    updateGreetingText();
    header('Location: engine.php?status=greeting_updated');
}

if(isset($_POST['get_start_en']))
{
    enableGetStartedBtn();
    header('Location: engine.php?status=get_start_en');
}

if(isset($_POST['get_start_dis']))
{
    disableGetStartedBtn();
    header('Location: engine.php?status=get_start_dis');;
}


?>
<?php set_page_title('Engine Config'); require_once 'header.php' ?>

<?php

if(isset($_GET['status']))
{
    switch ($_GET['status'])
    {
        case 'enabled':
            echo "<script>msg('Greeting text enabled successfully', 'success')</script>";
            break;
        case 'disabled':
            echo "<script>msg('Greeting text disabled successfully', 'success')</script>";
            break;
        case 'greeting_updated':
            echo "<script>msg('Greeting text updated successfully', 'success')</script>";
            break;


    }
}

?>

<div class="container">
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="col-md-8">
                    <h4><strong>Greeting Text</strong></h4>
                    <p>Redirect all requests with scheme 'http' to 'https'.</p>
                </div>
                <div class="col-md-4">
                    <div class="welcome-form">
                        <form action="engine.php" method="post" enctype="multipart/form-data">
                            <input type="text" name="greeting_text" id="greeting_text" value="<?= $config['greeting_text'] ?>">
                            <button type="submit" name="update_greeting_text" class="btn-primary" >Update</button>
                            <button type="button" class="btn-primary" onclick="copyToClipBoard('greeting_text')">Copy</button>
                            <p class="help-block note">Note: Disable and Enable is required after updating text.</p>
                        </form>

                    </div>
                </div>

                <div class="col-md-4 col-md-offset-8 welcome-btn">
                    <?php

                    ($config['greeting_text_status']) ? $btn = array('name'=>'Disable','state'=>'btn-danger') : $btn =  array('name'=>'Enable','state'=>'btn-primary');
                    ($config['greeting_text_status']) ? $btn_name = 'disable_welcome_text' : $btn_name = 'enable_welcome_text';
                    ($config['greeting_text_status']) ? $status = 'Enabled' : $status = 'Disabled' ;


                    ?>
                    <form action="engine.php" method="post" enctype="multipart/form-data">
                        <button type="submit" name="<?=  $btn_name ?>" class="<?= $btn['state'] ?>"><?= $btn['name'] ?></button>
                    </form>
                </div>

                <div class="col-md-4 col-md-offset-8 welcome-status">
                    Status : <strong><?= $status ?></strong>
                    <?php $btn=$btn_name=$status=null ?>
                </div>
            </div>
            <div class="panel-footer"><small><strong>Note:</strong> To Display Person's Name With Greeting Text Add <code>{{user_first_name}}</code> for First Name, <code>{{user_last_name}}</code> for Last Name and <code>{{user_full_name}}</code> for Full Name </small></div>
        </div>
    </div>




    <div class="row">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="col-md-8">
                    <h4><strong>Get Start Button</strong></h4>
                    <p>Redirect all requests with scheme 'http' to 'https'.</p>
                </div>

                <div class="col-md-4">
                    <?php

                    ($config['get_start_status']) ? $btn = array('name'=>'Disable','state'=>'btn-danger') : $btn =  array('name'=>'Enable','state'=>'btn-primary');
                    ($config['get_start_status']) ? $btn_name = 'get_start_dis' : $btn_name = 'get_start_en';
                    ($config['get_start_status']) ? $status = 'Enabled' : $status = 'Disabled';

                    ?>
                    <form action="engine.php" method="post" enctype="multipart/form-data">
                        <button type="submit" name="<?=  $btn_name ?>" class="<?= $btn['state'] ?> getstartbtn"><?= $btn['name'] ?></button>
                    </form>

                    <div>
                        Status : <strong><?= $status ?></strong>
                        <?php $btn=$btn_name=$status=null ?>
                    </div>
                </div>
            </div>
            <div class="panel-footer"></div>
        </div>
    </div>


    <div class="row">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="col-md-8">
                    <h4><strong>Hub Verify Token</strong></h4>
                    <p>Redirect all requests with scheme 'http' to 'https'.</p>
                </div>
                <div class="col-md-4">
                    <div class="welcome-form">
                        <form action="">
                            <input type="text" id="hub_verify_token" value="<?= $config['hub_verify_token'] ?>">
                            <button type="submit" class="btn-primary" >Update</button>
                            <button type="button" class="btn-primary" onclick="copyToClipBoard('hub_verify_token')">Copy</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="panel-footer"><small>Last modified on 15 Jan 2018</small></div>
        </div>
    </div>

    <div class="row">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="col-md-8">
                    <h4><strong>Access Token</strong></h4>
                    <p>Redirect all requests with scheme 'http' to 'https'.</p>
                </div>
                <div class="col-md-4">
                    <div class="welcome-form">
                        <form action="">
                            <input type="text" id="access_token"  value="<?= $config['access_token'] ?>">
                            <button type="submit" class="btn-primary" >Update</button>
                            <button type="button" class="btn-primary" onclick="copyToClipBoard('access_token')">Copy</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="panel-footer"></div>
        </div>
    </div>
</div>

<?php require_once 'footer.php' ?>
