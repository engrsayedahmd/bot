<?php
require_once 'app/fun/functions.php';
require_once 'app/message-controller.php';

$messages = messages();

if(isset($_POST['add_msg']))
{
    if(addMessage() == true)
    {
        header('Location: message.php?status=added');
    }
}

?>

<?php set_page_title('Message'); require_once 'header.php' ?>

<?php

if(isset($_GET['status']))
{
    switch ($_GET['status'])
    {
        case 'added':
            echo "<script>msg('Message added successfully', 'success')</script>";
            break;
    }
}

?>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <!-- Default panel contents -->
                <div class="panel-heading">Message</div>
                <div class="panel-body">

                    <div class="row">
                        <div class="col-md-6 col-md-offset-3">
                            <div class="panel panel-heading">
                                <form method="post" action="message.php" enctype="multipart/form-data">
                                    <label for="query">Query</label>
                                    <input type="text" name="query" id="query" required>
                                    <label for="response">Response</label>
                                    <textarea name="response" id="response" required></textarea>
                                    <button type="submit" name="add_msg" class="btn-primary">Add</button>
                                </form>
                            </div>
                        </div>
                    </div>

                </div>

                <table class="table table-bordered">
                    <tr>
                        <th>#</th>
                        <th>Query</th>
                        <th>Response</th>
                    </tr>
                    <?php foreach ($messages as $message):?>
                    <tr>
                        <td style="width: 70px">
                            <a href="#"><img src="assets/img/trash-alt-regular.svg" title="Delete" class="edit-delete" alt="Delete Button"></a>
                            <a href="#"><img src="assets/img/edit-regular.svg" title="Edit" class="edit-delete" alt="Edit Button"></a>
                        </td>
                        <td><?= $message['query'] ?></td>
                        <td><?= $message['response'] ?></td>
                    </tr>
                    <?php endforeach; ?>
                </table>
            </div>

        </div>
    </div>
</div>

<?php require_once 'footer.php' ?>