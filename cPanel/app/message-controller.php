<?php
/**
 * Created by PhpStorm.
 * User: hacker
 * Date: 7/16/18
 * Time: 8:53 AM
 */

require_once 'core/Core.php';
require_once 'db/db.php';

dbConfig($db);

function addMessage()
{
    $query = strtolower($_POST['query']);
    $response = $_POST['response'];

    $message_value = array(
        NULL,
        $query,
        $response
    );

    $tagging_value = array(
        NULL,
        $query,
        'message'
    );

    $tagging_status = storeInDB('tagging', $tagging_value);
    $message_status = storeInDB('message', $message_value);

    if($tagging_status['status']==='success' && $message_status['status']==='success')
    {
        return true;
    }
    else
    {
        return false;
    }
}

function messages()
{
    return fetchFromDB('message', 'DESC');
}

