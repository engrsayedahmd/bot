<?php
/**
 * Created by PhpStorm.
 * User: hacker
 * Date: 7/15/18
 * Time: 9:31 PM
 */

try {
   // $db = new PDO('mysql:host=localhost;dbname=sayedxyz_messenger;charset=utf8', 'sayedxyz_msnger', 'msnger@2018');
    $db = new PDO('mysql:host=localhost;dbname=messenger;charset=utf8', 'sayed', '12345');
} catch (PDOException $ex) {

    $message = array(
        'msg' => 'Error in database connection!',
        'contact' => 'engrsayedahmd@gmail.com'
    );

    header('Content-Type: application/json');
    echo json_encode($message, JSON_PRETTY_PRINT);
    exit;
}