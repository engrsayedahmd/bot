<?php
/**
 * Created by PhpStorm.
 * User: hacker
 * Date: 7/18/18
 * Time: 8:57 PM
 */

$page_title = null;

function get_page_title()
{
    global $page_title;
    return $page_title;
}

function set_page_title($title)
{
    global $page_title;
    $page_title = $title;
}
