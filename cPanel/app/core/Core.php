<?php
/**
 * Created by PhpStorm.
 * User: hacker
 * Date: 7/15/18
 * Time: 9:26 PM
 */

$db = null;

function dbConfig($db_config)
{
    global $db;

    $db = $db_config;
}

function storeInDB($table_name,$values)
{
    global $db;

    $params = array_fill(0, count($values), '?');

    $sql = "INSERT INTO ".$table_name." VALUES(".implode(',', $params).")";
    $stmt = $db->prepare($sql);
    $executed = $stmt->execute($values);
    return ($stmt->rowCount()) ? array('status'=>'success', 'executed' => $executed) : array('status'=>'failed', 'executed' => $executed);

}

function fetchFromDB($table_name, $order)
{
    global $db;

    $sql = "SELECT * FROM ".$table_name." ORDER BY id ".$order;
    $stmt = $db->prepare($sql);
    $stmt->execute();
    return $stmt->fetchAll();
}

function updateDB($table_name, $columns)
{
    global $db;

    $columns_name = array();

    foreach (array_keys($columns) as $column_name)
    {
        array_push($columns_name, $column_name.' = ?');
    }

    $sql = "UPDATE ".$table_name." SET ".implode(',',$columns_name);
    $stmt = $db->prepare($sql);
    $executed = $stmt->execute(array_values($columns));
    return ($stmt->rowCount()) ? array('status'=>'changed', 'executed' => $executed) : array('status'=>'unchanged', 'executed' => $executed);

}